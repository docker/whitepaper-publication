FROM python:3.9-slim-buster

# install make, latexmk, and LaTeX tools
RUN apt update && \
apt install --yes latexmk make texlive-latex-extra texlive-latex-recommended texlive-fonts-recommended && \
rm -rf /var/lib/apt/lists

# Install Sphinx
RUN pip3 install Sphinx==3.2.1
